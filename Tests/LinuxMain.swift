import XCTest
@testable import swifteredisTests

XCTMain([
    testCase(swifteredisTests.allTests),
])
